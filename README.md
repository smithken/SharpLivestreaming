# Cloud-based Live-Streaming from A-UGVs

A live-streaming system in three parts - IP camera endpoints streaming 
video and audio, a cloud-based server that ingests video and transcodes / splits 
into multiple streams, and a client application for viewing streams. 

## Cloud-based server

The cloud-based server makes use of a combination of AWS and Wowza Streaming 
Engine as the backend.  Wowza exposes a REST API for interacting with the 
backend server and provides a Streaming Engine Manager application for 
configuring streams.  

The Manager application is implemented on top of said REST API and provides 
all the functionality that we need.  This project simply implements a light, 
alternative interface to that functionality.  In this way, we can control 
access to the entirety of Wowza Streaming Engine, expose only the relevant 
configuration options, and streamline the user interface.

## Client application

A client application for playback of streams made available by the cloud-based 
server.  It allows simultaneous viewing of camera streams.

## Protocol

Initially, we considered the use of HTTP-based adaptive bitrate streaming 
protocols such as MPEG-DASH or Apple HLS.  However, these protocols impose a 
minimum latency of 6 seconds or so on a wired connection.  The reason being, 
these protocols require three chunks of video to be buffered 

Through experimentation and comparisons of network logs between test runs and 
live streams on Youtube / Twitch.tv, we found that lowering chunk duration 
below 2 seconds sees large diminishing returns in stream latency reduction.

Currently, we are testing WebRTC as an alternative.  Incidentally, Wowza has 
recently released BETA support for WebRTC, available to those with commercial 
licenses.  Wowza ingests WebRTC and splits it into multiple WebRTC streams.

In our preliminary tests on wired, local networks, WebRTC sees sub-second 
stream latency.  It's still unclear how to integrate this into Wowza ( we 
haven't tried ), and what the performance will look like once that happens. 

These preliminary tests were carried out using the uv4l-webrtc addon to 
uv4l.  Functionally, uv4l acts as a drop-in replacement for 
the RTSP server.  In both cases, the Raspberry Pi (A-UGV) runs a server 
from which a client can access a video stream.

## Build and run instructions

READMEs for each of Client application and Cloud-based server components 
can be found in their respective folders:

./app

and 

./servui

## Known Issues

The servui folder contains a file called BUGS, which describes 
known bugs and issues with the system as a whole.  Testing and further work 
on this system should take those issues (and possibly others) into account.