(function(){
    var App = angular.module('App', [
        "ngSanitize",
        "com.2fdevs.videogular",
        "com.2fdevs.videogular.plugins.controls",
        "com.2fdevs.videogular.plugins.overlayplay",
        "com.2fdevs.videogular.plugins.poster",
        "com.2fdevs.videogular.plugins.buffering",
        "com.2fdevs.videogular.plugins.dash",
        "ngRoute"
    ]);
    App.config(function($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl : "main.html"
            })
            .when("/stream", {
                templateUrl : "stream.html"
            })
            .when("/main", {
                templateUrl : "main.html"
            })
            .when("/hardcoded", {
                templateUrl : "hardcoded.html"
            });
    });
    App.service('dataService', function() {
        // private variable
        var _dataObj = [];
        // public API
        var dataObj = _dataObj;
        return {
            getDataObj: function () {
                return dataObj;
            },
            setDataObj: function(value) {
                dataObj = value;
            }
        };
    })
    

    App.controller('streamController', function($http, $scope, $sce, $timeout, dataService) {
        var controller = this;
        controller.state = null;
        controller.API = null;
        controller.currentVideo = 0;
        controller.onPlayerReady = function (API) {
            controller.API = API;
        };
        controller.onCompleteVideo = function () {
            controller.isCompleted = true;
            controller.currentVideo++;
            if (controller.currentVideo >= controller.videos.length) controller.currentVideo = 0;
            controller.setVideo(controller.currentVideo);
        };

        var data = dataService.getDataObj();

        controller.videos =  data;

        controller.config = {
            preload: "none",
            autoHide: false,
            autoHideTime: 3000,
            autoPlay: false,
            sources: controller.videos[0].sources,
            theme: {
                url: "resources/videogular.css"
            },
            plugins: {
                poster: "resources/SharpImage.png"
            }
        }
        controller.setVideo = function (index) {
            controller.API.stop();
            controller.currentVideo = index;
            controller.config.sources = controller.videos[index].sources;
            $timeout(controller.API.play.bind(controller.API), 100);
        };
    })

    App.controller('small0Controller', function($http, $scope, $sce, $timeout, dataService) {
        var controller = this;
        controller.state = null;
        controller.API = null;
        controller.currentVideo = 0;
        controller.onPlayerReady = function (API) {
            controller.API = API;
            controller.volume = 0;
        };
        controller.onCompleteVideo = function () {
            controller.isCompleted = true;
            controller.currentVideo++;
            if (controller.currentVideo >= controller.videos.length) controller.currentVideo = 0;
            controller.setVideo(controller.currentVideo);
        };

        var data = dataService.getDataObj();

        controller.videos = data;

        controller.config = {
            preload: "none",
            autoHide: false,
            autoHideTime: 3000,
            autoPlay: false,
            sources: controller.videos[0].sources,
            theme: {
                url: "resources/videogular.css"
            },
            plugins: {
                poster: "/resources/SharpImage.png"
            }
        }
    })
    App.controller('small1Controller', function($http, $scope, $sce, $timeout, dataService) {
        var controller = this;
        controller.state = null;
        controller.API = null;
        controller.currentVideo = 0;
        controller.onPlayerReady = function (API) {
            controller.API = API;
            controller.volume = 0;
        };
        controller.onCompleteVideo = function () {
            controller.isCompleted = true;
            controller.currentVideo++;
            if (controller.currentVideo >= controller.videos.length) controller.currentVideo = 0;
            controller.setVideo(controller.currentVideo);
        };

        var data = dataService.getDataObj();

        controller.videos = data;

        controller.config = {
            preload: "none",
            autoHide: false,
            autoHideTime: 3000,
            autoPlay: false,
            sources: controller.videos[1].sources,
            theme: {
                url: "resources/videogular.css"
            },
            plugins: {
                poster: "/resources/SharpImage.png"
            }
        }
    })
    App.controller('small2Controller', function($http, $scope, $sce, $timeout, dataService) {
        var controller = this;
        controller.state = null;
        controller.API = null;
        controller.currentVideo = 0;
        controller.onPlayerReady = function (API) {
            controller.API = API;
            controller.volume = 0;
        };
        controller.onCompleteVideo = function () {
            controller.isCompleted = true;
            controller.currentVideo++;
            if (controller.currentVideo >= controller.videos.length) controller.currentVideo = 0;
            controller.setVideo(controller.currentVideo);
        };

        var data = dataService.getDataObj();

        controller.videos = data;

        controller.config = {
            preload: "none",
            autoHide: false,
            autoHideTime: 3000,
            autoPlay: false,
            sources: controller.videos[2].sources,
            theme: {
                url: "resources/videogular.css"
            },
            plugins: {
                poster: "/resources/SharpImage.png"
            }
        }
    })

    App.controller('homeController', function($http, $scope, $sce, $timeout, dataService) {
        var controller = this;

        var url='http://localhost:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/applications';
        var RootURL='http://localhost:1935/';
        
        var streamfilelists= new Array();
        var streamlists =new Array();

        function GetID(application_id) {
            var shaObj = new jsSHA("SHA-1", "TEXT");
            shaObj.update(application_id);
            var hash = shaObj.getHash("HEX");
            return hash;
        }

        function GetParts(streamfile_name) {
            return streamfile_name.split("_");
        }


        controller.setVars = function (info) {

            $http.get(url)
                .success(function (response) {
                    $scope.currentStream = info;
                    $scope.currentStreamHash = GetID($scope.currentStream);
                    streamfilelists = [];
                })
                .then(function () {
                    $scope.test=$scope.data.map(function(){
                        return url+ "/"+ $scope.currentStream +"/streamfiles";
                    });
                })
                .then(function () {
                    $http.get(url+"/"+$scope.currentStream+"/streamfiles")
                        .success(function (response) {
                            $scope.StreamsRunning=response.streamFiles.map(function(obj) {
                                streamlists.push(RootURL+$scope.currentStream+'/'+obj.id+'.stream/manifest.mpd');
                                var currentStreamParts = GetParts(obj.id);
                                if(currentStreamParts[0] == $scope.currentStreamHash){
                                    streamfilelists.push({sources:[{src:RootURL+$scope.currentStream+'/'+ obj.id +'.stream/manifest.mpd'}]});
                                    return RootURL+$scope.currentStream+'/'+obj.id+'.stream/manifest.mpd';
                                }
                            });
                        })
                        .then(function(){
                            dataService.setDataObj(streamfilelists);
                        });
                })
        };


        $http.get(url)
            .success(function (response) {
                $scope.data=response.applications.map(function (obj) {
                    if(obj.appType==='Live'){
                        return obj.id;
                    }
                }).filter(function(n){ return n != undefined });
            })
            .then(function(){
                $scope.currentStream = $scope.data[0];
                $scope.currentStreamHash = GetID($scope.currentStream);
            })
            .then(function () {
                $scope.test=$scope.data.map(function(){
                    return url+ "/"+ $scope.currentStream +"/streamfiles";
                });
            })
            .then(function () {
                $http.get(url+"/"+$scope.currentStream+"/streamfiles")
                    .success(function (response) {
                        $scope.StreamsRunning=response.streamFiles.map(function(obj) {
                            streamlists.push(RootURL+$scope.currentStream+'/'+obj.id+'.stream/manifest.mpd');
                            var currentStreamParts = GetParts(obj.id);
                            if(currentStreamParts[0] == $scope.currentStreamHash){
                                streamfilelists.push({sources:[{src:RootURL+$scope.currentStream+'/'+ obj.id +'.stream/manifest.mpd'}]});
                                return RootURL+$scope.currentStream+'/'+obj.id+'.stream/manifest.mpd';
                            }
                        });
                    })
                    .then(function(){
                        console.log(streamfilelists);
                        dataService.setDataObj(streamfilelists);
                    });
            })
    })

})();