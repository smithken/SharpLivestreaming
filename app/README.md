# Client application

A client application for playback of streams made available by the cloud-based server. It allows simultaneous viewing of camera streams.

## How to Build

In /app: run 'npm install' 

In /app/bin: run 'node www', this starts the client server

navigate to "localhost:3000" in a browser.

# Note for Wowza developer license users:

Wowza developer license has a cap of 10 stream loads and a cap of 3 stream connections, so only 3 stream files can be connected at the same time. Also the app cannot load more than 10 streams (loads + 1 when a thumbnail is clicked).