# Servui (Cloud-based server)

## Notes

    Servui requires Wowza Streaming Engine 4.6.0 with a Developer's License.


## Auth settings

    Servui does not implement authentication to Wowza through its REST API calls.
    Therefore, before testing, the user should modify Server.xml at: 
    
    [Wowza Install Dir]/conf/Server.xml
    
    And make the following change from:
    
    <Server>
		...
		<RESTInterface>
            ...
			<!-- none, basic, digest, remotehttp, digestfile -->
			<AuthenticationMethod>digest</AuthenticationMethod>        <--here

    to:
    
    <Server>
		...
		<RESTInterface>
            ...
			<!-- none, basic, digest, remotehttp, digestfile -->
			<AuthenticationMethod>none</AuthenticationMethod>          <--here
    

## Core Dependencies
    
    1. Install nodejs
    
    2. Install bower
        $ npm install -g bower

## To Build

In /servui: 

    :$ npm install
    :$ bower install
    
## To Run

In /servui: 
    :$ node app.js [--host <servui hostname>] [--port <servui port>] [--wowzahost <wowza hostname>] [--wowzaport <wowza port>]  
    
    If no options are specified, defaults are:
        hostname = 'localhost'
        port = 10240
        wowza hostname = 'localhost'
        wowza port = 8087

    navigate to <host>:<port> in a browser
    
## Example Usages

    1. 
        :$ node app.js
        navigate to localhost:10240 in a browser
        
    2. 
        :$ node app.js --host 192.168.0.1 --port 10241
        navigate to 192.168.0.1:10241 in a browser
        
## Example Work Flow

    1. 
    
        * In the leftmost menu, click the "Add Car" button
        
        * Enter in a name for your A-UGV in the window
        
    2.  
        * In rightmost menu, click the button corresponding to the
            desired A-UGV.  For example, "Sharp1".
            
        * In the "Add a Stream File" window, enter in a streamfile 
            name (e.g "rtsp60") and the corresponding URL, and click the 
            "Create Streamfile" button.
            
        * In the "Sharp1:rtsp60" window, modify the URL as necessary and 
            click the "Update Stream Endpoint" button if need be. Else...
        
        * In the "Sharp1:rtsp60" window, click the "Start Streaming" 
            button.
            
        * Use the Client Application to view the stream for 
            Sharp1 by selecting it in the menu on the lefthand side of the 
            screen and then clicking the "Start Stream" button above.
            
        * In the "Sharp1:rtsp60" window, click the "Stop Streaming" button 
            to disconnect the stream.
            
        * In the "Sharp1:rtsp60" window, click the "Delete Stream Endpoint" 
            button to delete the stream endpoint.
        
## Testing

    Actions performed in the A-UGV can be double-checked in the 
    Wowza Streaming Engine Manager.
    
    Note that Wowza Streaming Engine Manager performs caching of 
    some sort during application (A-UGV / car) creation and deletion.
    
    You may need to close / reopen the browser window or clear the cache
    to see the results properly.  This does not appear to be an issue with 
    stream file manipulations.